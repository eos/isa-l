#!/usr/bin/env bash

EOS_CODENAME="diopside"

for arch in "el-7" "al-8" "al-9" ; do
    EXPORT_DIR=/eos/project/s/storage-ci/www/eos/${EOS_CODENAME}-depend/${arch}/x86_64/
    echo "Publishing for arch: ${arch} in location: ${EXPORT_DIR}"
    mkdir -p ${EXPORT_DIR}
    cp ${arch}_artifacts/*.rpm ${EXPORT_DIR}
    createrepo -q ${EXPORT_DIR}
done
